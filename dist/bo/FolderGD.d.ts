import { drive_v3 } from "googleapis";
export declare class FolderGD {
    drive: drive_v3.Drive;
    constructor();
    /**
     * Recebe um path e retorna o id.
     * Criará diretório e subdiretórios caso não existam
     *
     * @param path o caminho completo do diretório que deseja buscar. ex: '2019/08/20/manutencao/'
     * @param baseParentId (optional) se informado, buscará o path a partide desse parentid
     */
    obterIdToPath(options: obterIdToPath$Options): Promise<string>;
    /**
     *
     * @param keyParent o key da pasta na qual deseja que a nova pasta seja criada
     * @param path o caminho que deseja criar. ex: ['pasta1','pasta2'','pasta3'], criará
     * /-pasta1
     *    |-pasta2
     *       |-pasta3
     * @param drive
     */
    createFolder(options: {
        baseParentId: string;
        path: string;
    }): Promise<string>;
}
export interface obterIdToPath$Options {
    path: string;
    baseParentId?: string;
}
export interface createFolder$Options {
    baseParentId: string;
    path: string;
}
