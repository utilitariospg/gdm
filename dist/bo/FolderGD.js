"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var gdrivev3_1 = require("../util/gdrivev3");
var FolderGD = /** @class */ (function () {
    function FolderGD() {
        var _this = this;
        gdrivev3_1.getDrive().then(function (drive) {
            _this.drive = drive;
        }).catch(function (reason) {
            console.error("erro ao instanciar FolderGD");
            throw reason;
        });
    }
    /**
     * Recebe um path e retorna o id.
     * Criará diretório e subdiretórios caso não existam
     *
     * @param path o caminho completo do diretório que deseja buscar. ex: '2019/08/20/manutencao/'
     * @param baseParentId (optional) se informado, buscará o path a partide desse parentid
     */
    FolderGD.prototype.obterIdToPath = function (options) {
        var _this = this;
        var parentid;
        var pathList = options.path.split('/');
        parentid = options.baseParentId || process.env.GD_ROOT_ID;
        // removendo primeiro e ultimo elmento se os mesmos forem de tamanho ZERO
        if (pathList[0].length == 0) {
            pathList.shift();
        }
        else if (pathList[pathList.length - 1].length == 0) {
            pathList.pop();
        }
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var drive, currPath_1, _q, error_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, gdrivev3_1.getDrive()];
                    case 1:
                        drive = _a.sent();
                        currPath_1 = pathList.shift();
                        _q = "mimeType = 'application/vnd.google-apps.folder' and trashed = false";
                        _q = _q.concat(parentid ? " and '" + parentid + "' in parents" : '');
                        _q = _q.concat(" and name = '" + currPath_1 + "'");
                        drive.files.list({
                            q: _q,
                            fields: 'nextPageToken, files(id, name, parents)',
                            spaces: 'drive',
                        }).then(function (fileList) { return __awaiter(_this, void 0, void 0, function () {
                            var keyParent, file;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        if (!(fileList.data.files.length <= 0)) return [3 /*break*/, 4];
                                        if (!currPath_1) return [3 /*break*/, 2];
                                        pathList.unshift(currPath_1);
                                        return [4 /*yield*/, this.createFolder({ baseParentId: parentid, path: pathList.join('/') })];
                                    case 1:
                                        keyParent = _a.sent();
                                        resolve(keyParent);
                                        return [3 /*break*/, 3];
                                    case 2:
                                        resolve(parentid);
                                        _a.label = 3;
                                    case 3: return [3 /*break*/, 5];
                                    case 4:
                                        file = fileList.data.files[0];
                                        if (pathList.length > 0) {
                                            this.obterIdToPath({
                                                baseParentId: file.id,
                                                path: pathList.join('/')
                                            }).then(function (id) {
                                                resolve(id);
                                            });
                                        }
                                        else {
                                            resolve(file.id || parentid);
                                        }
                                        _a.label = 5;
                                    case 5: return [2 /*return*/];
                                }
                            });
                        }); }).catch(function (reason) {
                            throw reason;
                        });
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        reject(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     *
     * @param keyParent o key da pasta na qual deseja que a nova pasta seja criada
     * @param path o caminho que deseja criar. ex: ['pasta1','pasta2'','pasta3'], criará
     * /-pasta1
     *    |-pasta2
     *       |-pasta3
     * @param drive
     */
    FolderGD.prototype.createFolder = function (options) {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var pathList_1, currPath;
            var _this = this;
            return __generator(this, function (_a) {
                try {
                    pathList_1 = options.path.split('/');
                    // removendo primeiro e ultimo elmento se os mesmos forem de tamanho ZERO
                    if (pathList_1[0].length == 0) {
                        pathList_1.shift();
                    }
                    else if (pathList_1[pathList_1.length - 1].length == 0) {
                        pathList_1.pop();
                    }
                    currPath = pathList_1.shift();
                    this.drive.files.create({
                        requestBody: {
                            mimeType: 'application/vnd.google-apps.folder',
                            parents: [options.baseParentId],
                            name: currPath
                        }
                    }, function (err, resFile) {
                        if (err) {
                            return console.log("Erro ao tentar criar folder: ", err);
                        }
                        if (pathList_1.length > 0) {
                            _this.createFolder({ baseParentId: resFile.data.id, path: pathList_1.join('/') }).then(function (idFinal) {
                                resolve(idFinal);
                            }).catch(function (reason) {
                                reject(reason);
                            });
                        }
                        else {
                            resolve(resFile.data.id);
                        }
                    });
                }
                catch (reason) {
                    reject(reason);
                }
                return [2 /*return*/];
            });
        }); });
    };
    return FolderGD;
}());
exports.FolderGD = FolderGD;
// let foldeGd = new FolderGD();
// foldeGd.obterIdToPath({
//     path: '/2010/10/13/abastecimento'
// }).then((id) => {
//     console.info(`o id retornado foi: `, id)
// }).catch((reason) => {
//     return console.error(`erro geral`, reason)
// })
//# sourceMappingURL=FolderGD.js.map