/// <reference types="node" />
import * as fs from 'fs';
import { drive_v3 } from "googleapis";
export declare class FileGD {
    drive: drive_v3.Drive;
    constructor(drive?: drive_v3.Drive);
    /**
     * Busca arquivos no gdrive
     *
     * @param {string} q (opcional) query de busca.
     * @see [Query Documentation](https://developers.google.com/drive/api/v3/search-files)
     */
    findFile(q?: string): Promise<drive_v3.Schema$File[]>;
    /**
     * Cria arquivos no google drive
     *
     * @param parentsids os id's dos diretório nos quais o arquivo deve ser criado
     * @param filename o nome do arquivo, com extensão
     * @param readstream o readstream para os dados
     */
    createFile(options: createFile$Options): Promise<drive_v3.Schema$File>;
}
export interface createFile$Options {
    parentsids: string[];
    filename: string;
    mimeType: string;
    readstream: fs.ReadStream;
}
