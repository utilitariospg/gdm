import { drive_v3 } from 'googleapis';
import { OAuth2Client } from 'googleapis-common';
export declare function getDrive(): Promise<drive_v3.Drive>;
export declare function getAuth(): Promise<OAuth2Client>;
