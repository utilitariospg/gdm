# gdm

## Installing gdm (<a style="color: blue">G</a>oogle <a style="color: orange">D</a>rive <a style="color: red">M</a>anager)
```shel
$npm install pathfolder
```
or
```shel
$npm install git+https://gitlab.com/utilitariospg/gdm.git
```

## Using
```typescript
import { FileGD, 
         FolderGD, 
         obterIdToPath$Options, 
         createFile$Options } from 'gdm';

const foldergd = new FolderGD();
const filegd = new FileGD();

const KEYPARENT = 'ID_GOOGLEDRIVER_FOLDER'
const pathFolder = '/2019/08/01/dir1/dir2'
const fileName = 'SHEETS_2019.xls'

let options:obterIdToPath$Options = {
        path: pathFolder,
        baseParentId: KEYPARENT
    }

let idPathGDrive = await foldergd.obterIdToPath(options);

const options:createFile$Options = {
    parentsids: [idPathGDrive],
    filename: fileName,
    mimeType: 'text/html',
    readstream: fs.createReadStream(pathFile)
}
const resultOfCreation = await filegd.createFile(cfOptions)

console.log(JSON.string(resultOfCreation))
```