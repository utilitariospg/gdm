import * as fs from 'fs';
import { drive_v3, google } from 'googleapis';
import { OAuth2Client } from 'googleapis-common';
import * as readline from 'readline';

const TOKEN_PATH = 'token.json';
const SCOPES = ['https://www.googleapis.com/auth/drive.file'];

export function getDrive():Promise<drive_v3.Drive> {
    return new Promise<drive_v3.Drive>(async (resolve, reject) => {
        try {
            let auth = await getAuth();
            let drive = new drive_v3.Drive({auth: auth});
            resolve(drive) 
        } catch (error) {
            reject(error)
        }
    })
}

export function getAuth(): Promise<OAuth2Client> {
    return new Promise<OAuth2Client>(async (resolve, reject) => {
        const credentials = await getCredentialsFile();
        const { client_secret, client_id, redirect_uris } = credentials.installed;
        const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
        try {
            fs.readFile(TOKEN_PATH, (err, token) => {
                if (err) return getAccessToken(oAuth2Client);
                oAuth2Client.setCredentials(JSON.parse(token.toString()));
                return resolve(oAuth2Client)
            });
        } catch (reason) {
            return reject(reason)
        }
    })
}

function getCredentialsFile(): Promise<any> {
    return new Promise((resolve, reject) => {
        fs.readFile('credentials.json', (err, content) => {
            if (err) {
                return reject(`Erro loading client secrete file: ${err}`)
            }
            resolve(JSON.parse(content.toString()))
        })
    })
}

function getAccessToken(oAuth2Client): Promise<any> {
    return new Promise((resolve, reject) => {
        const authUrl = oAuth2Client.generateAuthUrl({
            access_type: 'offline',
            scope: SCOPES,
        });
        console.log('Authorize this app by visiting this url:', authUrl);
        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout,
        });
        rl.question('Enter the code from that page here: ', (code) => {
            rl.close();
            oAuth2Client.getToken(code, (err, token) => {
                if (err) return console.error('Error retrieving access token', err);
                oAuth2Client.setCredentials(token);
                resolve(oAuth2Client)
                // Store the token to disk for later program executions
                fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                    if (err) return console.error(err);
                    console.log('Token stored to', TOKEN_PATH);
                });
            });
        });
    })
}