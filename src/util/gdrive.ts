// import * as fs from 'fs';
// import { drive_v3, google } from 'googleapis';
// import * as readline from 'readline';

// // If modifying these scopes, delete token.json.
// const SCOPES = ['https://www.googleapis.com/auth/drive.file'];
// // The file token.json stores the user's access and refresh tokens, and is
// // created automatically when the authorization flow completes for the first
// // time.
// const TOKEN_PATH = 'token.json';


// export function createFileGDrive(pathFile, nameFile) {
//     // Load client secrets from a local file.
//     fs.readFile('credentials.json', (err, content) => {
//         if (err) return console.log('Error loading client secret file:', err);
//         // Authorize a client with credentials, then call the Google Drive API.
//         authorize(JSON.parse(content.toString()), pathFile, nameFile, _createFile);
//     });
// }

// export function createFolderGDrive(pathFile, nameFile) {
//     // Load client secrets from a local file.
//     fs.readFile('credentials.json', (err, content) => {
//         if (err) return console.log('Error loading client secret file:', err);
//         // Authorize a client with credentials, then call the Google Drive API.
//         authorize(JSON.parse(content.toString()), pathFile, nameFile, _createAFolder);
//     });
// }

// /**
//  * Create an OAuth2 client with the given credentials, and then execute the
//  * given callback function.
//  * @param {Object} credentials The authorization client credentials.
//  * @param {function} callback The callback to call with the authorized client.
//  */
// function authorize(credentials, pathFile, nameFile, callback) {
//     const { client_secret, client_id, redirect_uris } = credentials.installed;
//     const oAuth2Client = new google.auth.OAuth2(
//         client_id, client_secret, redirect_uris[0]);

//     // Check if we have previously stored a token.
//     fs.readFile(TOKEN_PATH, (err, token) => {
//         if (err) return getAccessToken(oAuth2Client, pathFile, nameFile, callback);
//         oAuth2Client.setCredentials(JSON.parse(token.toString()));
//         callback(pathFile, nameFile, oAuth2Client);
//     });
// }

// /**
//  * Get and store new token after prompting for user authorization, and then
//  * execute the given callback with the authorized OAuth2 client.
//  * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
//  * @param {getEventsCallback} callback The callback for the authorized client.
//  */
// function getAccessToken(oAuth2Client, pathFile, nameFile, callback) {
//     const authUrl = oAuth2Client.generateAuthUrl({
//         access_type: 'offline',
//         scope: SCOPES,
//     });
//     console.log('Authorize this app by visiting this url:', authUrl);
//     const rl = readline.createInterface({
//         input: process.stdin,
//         output: process.stdout,
//     });
//     rl.question('Enter the code from that page here: ', (code) => {
//         rl.close();
//         oAuth2Client.getToken(code, (err, token) => {
//             if (err) return console.error('Error retrieving access token', err);
//             oAuth2Client.setCredentials(token);
//             // Store the token to disk for later program executions
//             fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
//                 if (err) return console.error(err);
//                 console.log('Token stored to', TOKEN_PATH);
//             });
//             callback(pathFile, nameFile, oAuth2Client);
//         });
//     });
// }

// function _createFile(pathFIle, nameFile, auth) {
//     var media = {
//         mimeType: 'text/html',
//         body: fs.createReadStream(pathFIle),
//     };
//     const drive = new drive_v3.Drive({
//         auth: auth
//     })
//     drive.files.create({
//         requestBody: {
//             parents: ['1QhGYRXODs8JMQ6p-dcEZs0W1WFcZpaWo'],
//             name: nameFile
//         },
//         media: media,
//         fields: 'id',
//     }, function (err, file) {
//         if (err) {
//             // Handle error
//             console.error(err);
//         } else {
//             console.log('File Id: ', JSON.stringify(file.data.id));
//         }
//     });
// }

// function _createAFolder(pathFolder, auth): Promise<string> {
//     return new Promise<string>((resolve, reject) => {
//         try {
//             var fileMetadata = {
//                 'name': pathFolder,
//                 'mimeType': 'application/vnd.google-apps.folder'
//             };

//             const drive = new drive_v3.Drive({
//                 auth: auth
//             })

//             drive.files.create({
//                 requestBody: fileMetadata,
//                 fields: 'id'
//             }, function (err, file) {
//                 if (err) {
//                     // Handle error
//                     console.error(err);
//                 } else {
//                     console.log('Folder Id: ', file.id);
//                     resolve(file.id)
//                 }
//             });
//         } catch (error) {
//             reject(error)
//         }
//     })
// }

// /**
//  * Lists the names and IDs of up to 10 files.
//  * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
//  */
// function listFiles(auth) {
//     const drive = google.drive({ version: 'v3', auth });
//     drive.files.list({
//         pageSize: 10,
//         fields: 'nextPageToken, files(id, name)',
//     }, (err, res) => {
//         if (err) return console.log('The API returned an error: ' + err);
//         if (res) {
//             const files = res.data.files;
//             if (files && files.length) {
//                 console.log('Files:');
//                 files.map((file) => {
//                     console.log(`${file.name} (${file.id})`);
//                 });
//             } else {
//                 console.log('No files found.');
//             }
//         }
//     });
// }