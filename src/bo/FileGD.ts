import * as fs from 'fs';
import { drive_v3 } from "googleapis";
import { getDrive } from "../util/gdrivev3";

export class FileGD {

    drive: drive_v3.Drive;

    constructor(drive?: drive_v3.Drive) {
        if (!drive) {
            getDrive().then((drive) => {
                this.drive = drive
            }).catch((reason) => {
                console.error(`erro ao instanciar FileGD`)
                throw reason
            })
        } else {
            this.drive = drive
        }
    }

    /**
     * Busca arquivos no gdrive
     * 
     * @param {string} q (opcional) query de busca.
     * @see [Query Documentation](https://developers.google.com/drive/api/v3/search-files)
     */
    findFile(q?: string): Promise<drive_v3.Schema$File[]> {
        return new Promise<drive_v3.Schema$File[]>(async (resolve, reject) => {

            if (!q) {
                q = ''
            }

            this.drive.files.list({
                q: q,
                fields: 'nextPageToken, files(id, name, parents, mimeType)',
                spaces: 'drive'
            }, function (err, res) {
                if (err) {
                    console.error(err);
                    return reject(err)
                }
                if (res) {
                    resolve(res.data.files)
                } else {
                    resolve()
                }
            });
        })
    }

    /**
     * Cria arquivos no google drive
     * 
     * @param parentsids os id's dos diretório nos quais o arquivo deve ser criado
     * @param filename o nome do arquivo, com extensão
     * @param readstream o readstream para os dados
     */
    createFile(options: createFile$Options): Promise<drive_v3.Schema$File> {
        return new Promise<drive_v3.Schema$File>(async (resolve, reject) => {
            try {
                this.drive.files.create({
                    requestBody: {
                        parents: options.parentsids,
                        name: options.filename
                    },
                    media: {
                        mimeType: options.mimeType,
                        body: options.readstream,
                    },
                    fields: 'id',
                }, function (err, res) {
                    if (err) {
                        console.error(err);
                        throw err;
                    }
                    if (res && res.status === 200) {
                        resolve(res.data);
                    } else {
                        reject(res)
                    }
                });
            } catch (error) {
                return reject(error)
            }
        });
    }
}

export interface createFile$Options { 
    parentsids: string[], 
    filename: string, 
    mimeType: string, 
    readstream: fs.ReadStream 
}