import { drive_v3 } from "googleapis";
import { getDrive } from "../util/gdrivev3";

export class FolderGD {

    drive: drive_v3.Drive;

    constructor() {
        getDrive().then((drive) => {
            this.drive = drive
        }).catch((reason) => {
            console.error(`erro ao instanciar FolderGD`)
            throw reason
        })
    }

    /**
     * Recebe um path e retorna o id. 
     * Criará diretório e subdiretórios caso não existam
     * 
     * @param path o caminho completo do diretório que deseja buscar. ex: '2019/08/20/manutencao/'
     * @param baseParentId (optional) se informado, buscará o path a partide desse parentid
     */
    obterIdToPath(options: obterIdToPath$Options): Promise<string> {

        let parentid: string;
        let pathList: string[] = options.path.split('/');

        parentid = options.baseParentId || process.env.GD_ROOT_ID
        // removendo primeiro e ultimo elmento se os mesmos forem de tamanho ZERO
        if (pathList[0].length == 0) {
            pathList.shift()
        } else if (pathList[pathList.length - 1].length == 0) {
            pathList.pop()
        }

        return new Promise<string>(async (resolve, reject) => {
            try {
                const drive = await getDrive();
                const currPath = pathList.shift();

                let _q: string = `mimeType = 'application/vnd.google-apps.folder' and trashed = false`;
                _q = _q.concat(parentid ? ` and '${parentid}' in parents` : '')
                _q = _q.concat(` and name = '${currPath}'`)

                drive.files.list({
                    q: _q,
                    fields: 'nextPageToken, files(id, name, parents)',
                    spaces: 'drive',
                }).then(async (fileList) => {
                    if (fileList.data.files.length <= 0) { // nao existe
                        if (currPath) {
                            pathList.unshift(currPath)
                            let keyParent = await this.createFolder({ baseParentId: parentid, path: pathList.join('/') });
                            resolve(keyParent)
                        } else {
                            resolve(parentid)
                        }
                    } else {
                        let file = fileList.data.files[0]
                        if (pathList.length > 0) {
                            this.obterIdToPath({
                                baseParentId: file.id,
                                path: pathList.join('/')
                            }).then((id) => {
                                resolve(id)
                            })
                        } else {
                            resolve(file.id || parentid)
                        }
                    }
                }).catch((reason) => {
                    throw reason;
                })
            } catch (error) {
                reject(error)
            }
        })
    }

    /**
     * 
     * @param keyParent o key da pasta na qual deseja que a nova pasta seja criada
     * @param path o caminho que deseja criar. ex: ['pasta1','pasta2'','pasta3'], criará 
     * /-pasta1
     *    |-pasta2
     *       |-pasta3
     * @param drive 
     */
    createFolder(options: { baseParentId: string, path: string }): Promise<string> {
        return new Promise<string>(async (resolve, reject) => {
            try {

                let pathList: string[] = options.path.split('/');
                // removendo primeiro e ultimo elmento se os mesmos forem de tamanho ZERO
                if (pathList[0].length == 0) {
                    pathList.shift()
                } else if (pathList[pathList.length - 1].length == 0) {
                    pathList.pop()
                }

                const currPath = pathList.shift();
                this.drive.files.create({
                    requestBody: {
                        mimeType: 'application/vnd.google-apps.folder',
                        parents: [options.baseParentId],
                        name: currPath
                    }
                }, (err, resFile) => {
                    if (err) {
                        return console.log(`Erro ao tentar criar folder: `, err);
                    }

                    if (pathList.length > 0) {
                        this.createFolder({ baseParentId: resFile.data.id, path: pathList.join('/') }).then((idFinal: string) => {
                            resolve(idFinal)
                        }).catch((reason) => {
                            reject(reason)
                        })
                    } else {
                        resolve(resFile.data.id)
                    }
                })
            } catch (reason) {
                reject(reason)
            }
        })
    }
}

export interface obterIdToPath$Options {
    path: string,
    baseParentId?: string
}

export interface createFolder$Options {
    baseParentId: string, 
    path: string 
}
// let foldeGd = new FolderGD();
// foldeGd.obterIdToPath({
//     path: '/2010/10/13/abastecimento'
// }).then((id) => {
//     console.info(`o id retornado foi: `, id)
// }).catch((reason) => {
//     return console.error(`erro geral`, reason)
// })